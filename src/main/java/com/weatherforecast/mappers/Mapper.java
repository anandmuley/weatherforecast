package com.weatherforecast.mappers;

public interface Mapper<S, T> {

    T map(S s);

}

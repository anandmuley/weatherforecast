package com.weatherforecast.mappers;

import com.weatherforecast.domain.HourlyForecast;
import com.weatherforecast.domain.WeatherForecast;
import com.weatherforecast.dtos.WeatherForecastDto;
import com.weatherforecast.dtos.WeatherForecastRecordDto;
import org.springframework.stereotype.Component;

@Component
public class ResponseMapper implements Mapper<WeatherForecastDto, WeatherForecast> {

    @Override
    public WeatherForecast map(WeatherForecastDto response) {
        WeatherForecast weatherForecast = new WeatherForecast();
        response.list.forEach((WeatherForecastRecordDto recordDto) -> {
            weatherForecast.addForecastRecord(new HourlyForecast(recordDto.dt_txt, recordDto.main.temp));
        });
        return weatherForecast;
    }
}

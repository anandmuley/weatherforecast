package com.weatherforecast.repositories;

import com.weatherforecast.domain.Postcode;
import com.weatherforecast.domain.WeatherForecast;

public interface WeatherForecastRepository {

    WeatherForecast getBy(Postcode postcode) throws RuntimeException;

}
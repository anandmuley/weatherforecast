package com.weatherforecast.repositories;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.weatherforecast.domain.Postcode;
import com.weatherforecast.domain.WeatherForecast;
import com.weatherforecast.dtos.WeatherForecastDto;
import com.weatherforecast.mappers.Mapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Repository
public class OpenWeatherAPIRepository implements WeatherForecastRepository {

    private final WebResource webResource;
    private final Mapper<WeatherForecastDto, WeatherForecast> mapper;

    @Value("${forecastapi.noOfRecords}")
    private String noOfRecords;

    @Value("${forecastapi.unit}")
    private String unit;

    public OpenWeatherAPIRepository(Mapper mapper, @Value("${forecastapi.url}") String apiUrl, @Value("${forecastapi.key}") String apiKey) {
        Client client = Client.create();
        webResource = client.resource(UriBuilder.fromUri(apiUrl).queryParam("APPID", apiKey).build().toString());
        this.mapper = mapper;
    }

    @Override
    public WeatherForecast getBy(Postcode postcode) throws RuntimeException {
        MultivaluedMap<String, String> params = new MultivaluedMapImpl();
        params.add("zip", postcode.getValue().toString());
        params.add("cnt", noOfRecords);
        params.add("units", unit);
        ClientResponse response = webResource.queryParams(params).accept(APPLICATION_JSON)
                .get(ClientResponse.class);
        if (response.getStatus() != 200) {
            throw new RuntimeException("Unable to fetch details"
                    + response.getStatus());
        }
        return mapper.map(response.getEntity(WeatherForecastDto.class));
    }

}

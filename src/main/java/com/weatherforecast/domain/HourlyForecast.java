package com.weatherforecast.domain;

public class HourlyForecast extends ForecastRecord implements Comparable<HourlyForecast> {

    public HourlyForecast(String dateTime, Double temperature) {
        super(dateTime, temperature);
    }

    @Override
    public int compareTo(HourlyForecast that) {
        int result = 0;
        if (this.temperature < that.temperature) {
            result = -1;
        } else if (this.temperature > that.temperature) {
            result = 1;
        }
        return result;
    }
}

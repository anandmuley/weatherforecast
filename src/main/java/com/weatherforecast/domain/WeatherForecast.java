package com.weatherforecast.domain;

import java.util.ArrayList;
import java.util.List;

public class WeatherForecast {

    private List<ForecastRecord> forecastRecords;

    public WeatherForecast() {
        this.forecastRecords = new ArrayList<>();
    }

    public void addForecastRecord(ForecastRecord forecastRecord) {
        forecastRecords.add(forecastRecord);
    }

    public Integer getNoOfRecords() {
        return forecastRecords.size();
    }

    public List<ForecastRecord> getForecastRecords() {
        return forecastRecords;
    }


}

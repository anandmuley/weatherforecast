package com.weatherforecast.domain;

public class Postcode {

    private Integer value;

    private Postcode(Integer value) {
        this.value = value;
    }

    public static Postcode createNew(Integer value){
        return new Postcode(value);
    }

    public Integer getValue() {
        return value;
    }

}

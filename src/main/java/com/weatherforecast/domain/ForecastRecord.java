package com.weatherforecast.domain;

public abstract class ForecastRecord {

    public enum Unit {
        METRIC, FARENHEIT
    }

    protected String dateTime;
    protected Double temperature;
    protected Unit unit = Unit.METRIC;

    public ForecastRecord(String dateTime, Double temperature) {
        this.dateTime = dateTime;
        this.temperature = temperature;
    }

    public String getDateTime() {
        return dateTime;
    }

    public Double getTemperature() {
        return temperature;
    }
}

package com.weatherforecast.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecastDto {
    public String cod;
    public Long cnt;
    public List<WeatherForecastRecordDto> list;

}

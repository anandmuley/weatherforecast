package com.weatherforecast.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherForecastRecordDto {

    public String dt_txt;
    public ForecastRecordDetailsDto main;

}

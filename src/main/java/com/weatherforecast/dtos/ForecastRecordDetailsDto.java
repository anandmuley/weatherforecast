package com.weatherforecast.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastRecordDetailsDto {

    public Double temp;

}

package com.weatherforecast.resources;

import com.weatherforecast.domain.Postcode;
import com.weatherforecast.domain.WeatherForecast;
import com.weatherforecast.dtos.response.ErrorResponseDto;
import com.weatherforecast.repositories.WeatherForecastRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("weather")
@Component
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
@Api(value = "weather", description = "Weather forecast data")
public class WeatherResource {

    @Autowired
    private WeatherForecastRepository weatherForecastRepository;

    @GET
    @Path("{postcode}/forecast")
    @ApiOperation(value = "GET forecast by postcode", notes = "The endpoint is used to fetch the forecast details by postcode",
            produces = APPLICATION_JSON)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Matching forecast details are returned", response = WeatherForecast.class),
            @ApiResponse(code = 400, message = "Invalid postcode or no matching records found", response = ErrorResponseDto.class),
            @ApiResponse(code = 500, message = "Something went wrong due to internal server error", response = ErrorResponseDto.class)})
    public Response getForecastByPostcode(@PathParam("postcode") Integer postcode) {
        Response response;
        try {
            response = Response.ok(weatherForecastRepository.getBy(Postcode.createNew(postcode))).build();
        } catch (RuntimeException e) {
            response = Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponseDto(e.getMessage())).build();
        }
        return response;
    }

}

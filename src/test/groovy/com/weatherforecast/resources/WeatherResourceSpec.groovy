package com.weatherforecast.resources

import com.weatherforecast.domain.Postcode
import com.weatherforecast.domain.WeatherForecast
import com.weatherforecast.dtos.response.ErrorResponseDto
import com.weatherforecast.repositories.WeatherForecastRepository
import spock.lang.Specification
import spock.lang.Subject

import javax.ws.rs.core.Response

class WeatherResourceSpec extends Specification {

    @Subject
    WeatherResource resource

    WeatherForecastRepository mockWeatherForecastRepository

    def setup() {
        enableStrictMock()
        mockWeatherForecastRepository = Mock(WeatherForecastRepository)
        resource = new WeatherResource()
        resource.weatherForecastRepository = mockWeatherForecastRepository
    }

    void enableStrictMock() {
        0 * _
    }

    def "getForecastByPostcode - should return the matching records"() {
        given:
        def postcode = 46060
        WeatherForecast weatherForecast = new WeatherForecast()
        1 * mockWeatherForecastRepository.getBy({ Postcode it ->
            assert it.value == postcode
            true
        }) >> weatherForecast

        when:
        Response actual = resource.getForecastByPostcode(postcode)

        then:
        actual.status == 200
        ((WeatherForecast) actual.entity).noOfRecords == 0
    }

    def "getForecastByPostcode - should return error if error occurred"() {
        given:
        def postcode = 46060
        RuntimeException ex = new RuntimeException("Something went wrong")
        1 * mockWeatherForecastRepository.getBy({ Postcode it ->
            assert it.value == postcode
            true
        }) >> { throw ex }

        when:
        Response actual = resource.getForecastByPostcode(postcode)

        then:
        actual.status == 400
        ((ErrorResponseDto)actual.entity).message == ex.message
    }

}

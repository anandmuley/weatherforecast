package com.weatherforecast.repositories

import com.sun.jersey.api.client.Client
import com.sun.jersey.api.client.WebResource
import com.weatherforecast.domain.Postcode
import com.weatherforecast.domain.WeatherForecast
import com.weatherforecast.mappers.Mapper
import spock.lang.Specification
import spock.lang.Subject

class OpenWeatherAPIRepositorySpec extends Specification {

    @Subject
    OpenWeatherAPIRepository openWeatherAPIRepository

    Mapper mockMapper
    WebResource mockWebResource

    String apiUrl
    String apiKey

    def setup() {
        0 * _
        // TODO Need to see why this static method mocking is not working
        GroovyMock(Client, global: true)
        apiUrl = "https://api.openweathermap.org/data/2.5/forecast"
        apiKey = "98046abae4914355c0856cb3b042fd7d"
        mockMapper = Mock(Mapper)
        mockWebResource = Mock(WebResource)
        openWeatherAPIRepository = new OpenWeatherAPIRepository(mockMapper, apiUrl, apiKey)
    }

    def "getBy - should return the forecast result by postcode"() {
        given:
        Postcode postcode = Postcode.createNew(46060)
        def noOfRecords = 40
        WeatherForecast weatherForecast = new WeatherForecast()
        1 * mockMapper.map({ it ->
            assert it.cod == "200"
            assert it.cnt == noOfRecords
            true
        }) >> weatherForecast

        when:
        WeatherForecast actual = openWeatherAPIRepository.getBy(postcode)

        then:
        actual == weatherForecast

    }

    def "getBy - should throw an exception if an error occurred"() {
        given:
        Postcode postcode = Postcode.createNew(123)

        when:
        openWeatherAPIRepository.getBy(postcode)

        then:
        thrown(RuntimeException)

    }


}

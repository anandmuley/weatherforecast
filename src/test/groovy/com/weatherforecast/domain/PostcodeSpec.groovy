package com.weatherforecast.domain

import spock.lang.Specification
import spock.lang.Subject

class PostcodeSpec extends Specification {

    @Subject
    Postcode postcode

    def "constructor - should construct postcode object when valid postcode is provided"(){
        given:
        def givenValue = 94040

        when:
        postcode = new Postcode(givenValue)

        then:
        postcode.value == 94040
    }
}

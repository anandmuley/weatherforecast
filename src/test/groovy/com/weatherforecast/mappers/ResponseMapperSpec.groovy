package com.weatherforecast.mappers

import com.weatherforecast.domain.ForecastRecord
import com.weatherforecast.domain.WeatherForecast
import com.weatherforecast.dtos.ForecastRecordDetailsDto
import com.weatherforecast.dtos.WeatherForecastDto
import com.weatherforecast.dtos.WeatherForecastRecordDto
import spock.lang.Specification
import spock.lang.Subject

class ResponseMapperSpec extends Specification {

    @Subject
    ResponseMapper mapper

    def setup() {
        mapper = new ResponseMapper()
    }

    def "map - should map the required details from the response"() {
        given:
        def response = new WeatherForecastDto("list": [
                new WeatherForecastRecordDto(dt_txt: "2018-12-13 12:00:00", main: new ForecastRecordDetailsDto(temp: 270.93)),
                new WeatherForecastRecordDto(dt_txt: "2018-12-13 15:00:00", main: new ForecastRecordDetailsDto(temp: 275.91)),
                new WeatherForecastRecordDto(dt_txt: "2018-12-13 18:00:00", main: new ForecastRecordDetailsDto(temp: 282.47))
        ])

        when:
        WeatherForecast actual = mapper.map(response)

        then:
        actual != null
        actual.noOfRecords == 3
        actual.forecastRecords[0].eachWithIndex { ForecastRecord entry, int index ->
            entry.dateTime == response.list[index].dt_txt
        }

    }


}

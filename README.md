# Introduction
The app fetches the current weather details from the open weather api based on the postcode. Currently it supports only
USA region search

# Technology and frameworks

Below are the technologies and frameworks used

 Name | Version
 -----|--------
 Java | 1.8.0_181
 Gradle | 2.14


# Setup
In the bash terminal run the below command

```
gradle clean tomcatRun
```
